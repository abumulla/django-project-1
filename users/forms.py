from django import forms
from .models import Users

class UserProfileForm(forms.ModelForm):
    class Meta:
        model = Users
        fields = ['name','email','password']
