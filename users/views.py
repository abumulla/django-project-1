from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from users.models import Users
from users.serializer import UsersSerializer

# Create your views here.
class RegisterView(APIView):
    def post(self, request):
        data = UsersSerializer(data=request.data)
        try:
            if data.is_valid():
                email = data.validated_data['email']
                print(email)
                print(Users.objects.get(email = email))
                return Response({
                        "message": "Email already in use, try with other email!"
                        }, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            if data.is_valid():
                data.save()
                print("data saved!")
                return Response({'message' : 'Registered Successfull!!'}, status=status.HTTP_201_CREATED)
            else:
                return Response({
                    'message' : 'Invalid data, register again!'
                }, status=status.HTTP_400_BAD_REQUEST)
