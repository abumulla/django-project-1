document.getElementById('registerForm').addEventListener('submit', async function(event) {
    event.preventDefault();

    const name = document.getElementById('id_name').value;
    const email = document.getElementById('id_email').value;
    const password = document.getElementById('id_password').value;

    try {
        const response = await fetch('/api/register', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name : name,
                email : email,
                password : password
            })
        });

        const data = await response.json();

        alert(data.message);

    } catch (error) {
        console.error('Error:', error);
        alert('An error occurred. Please try again.');
    }
    this.reset();
});