from django.shortcuts import render

from users.forms import UserProfileForm

# Create your views here.
def signupPage(request):
    form = UserProfileForm()
    return render(request,"signUp.html",{
        "form":form
    })