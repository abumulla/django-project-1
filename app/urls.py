from django.urls import path

from app.views import signupPage

urlpatterns = [
    path('signup', signupPage),
]
